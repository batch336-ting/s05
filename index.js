class Customer {
    constructor(email) {
      this.email = email;
      this.cart = new Cart();
      this.orders = [];
    }
  
    checkOut() {
        if (this.cart.contents.length > 0) {
          this.orders.push({
            products: this.cart.contents,
            totalAmount: this.cart.totalAmount,
          });
        }
        return this; 
    }
}
  

class Product {
    constructor(name, price) {
      this.name = name;
      this.price = price;
      this.isActive = true;
    }
  
    archive() {
      this.isActive = false;
      return prodA;
    }
  
    updatePrice(newPrice) {
      this.price = newPrice;
      return prodA;
    }
}
  
  
class Cart {
    constructor() {
      this.contents = [];
      this.totalAmount = 0;
    }
  
    addToCart(product, quantity) {
      this.contents.push({ product, quantity });
      return this   ;
    }
  
    showCartContents() {
      return this.contents;
    }
  
    updateProductQuantity(productName, newQuantity) {
      const itemIndex = this.contents.findIndex(
        (item) => item.product.name === productName
      );
  
      if (itemIndex !== -1) {
        this.contents[itemIndex].quantity = newQuantity;
        return this;
      }
    }
  
    clearCartContents() {
      this.contents = [];
      return this;
    }
  
    computeTotal() {
        this.totalAmount = this.contents.reduce((total, item) => {
          return total + item.product.price * item.quantity;
        }, 0);
        return this; 
    }
}


const john = new Customer('john@mail.com');
const prodA = new Product('Shoes', 9.99);
  





    